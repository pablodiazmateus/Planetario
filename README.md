[![Swift Version][swift-image]][swift-url]

[swift-image]:https://img.shields.io/badge/swift-5.0-orange.svg
[swift-url]: https://swift.org/

# Planetario

This is an iOS app for iPad. Created by Alex de la Rosa. 

Planetario Alfa has several aquariums, there are iPads where visitors can read more about the species that live there.
