//
//  MenuCollectionViewCell.swift
//  Planetario
//
//  Created by Alex De la Rosa on 17/12/16.
//  Copyright © 2016 Alex De la Rosa. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var fishImage: UIImageView!
    @IBOutlet private weak var fishName: UILabel!
    
    func setCellForFishTank() {
        self.contentView.roundAllCorners()
        self.fishName.text = FishTank.sharedInstance.fishTankName
        self.fishImage.image = UIImage(named: FishTank.sharedInstance.type.getBackgroundImageName())
    }
    
    func setCell(for fish: Fish) {
        self.contentView.roundAllCorners()
        self.fishName.text = fish.name
        self.fishImage.image = UIImage(named: fish.photoName)
    }
}
