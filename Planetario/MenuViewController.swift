//
//  MenuViewController.swift
//  Planetario
//
//  Created by Alex De la Rosa on 14/12/16.
//  Copyright © 2016 Alex De la Rosa. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    private var selectedIndex = -1

    override func viewDidLoad() {
        let navbarFont = UIFont(name: "Raleway-Bold", size: 30) ?? UIFont.systemFont(ofSize: 30)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navbarFont, NSAttributedString.Key.foregroundColor: UIColor.fishTankTitleColor]
        self.collectionView.backgroundView = UIImageView(image: UIImage(named: FishTank.sharedInstance.type.getBackgroundImageName()))
        self.collectionView.register(UINib(nibName: "MenuCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "fishCell")
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = FishTank.sharedInstance.fishTankName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func createFishSlideShow() {
        let fishInfoVC = TabPageViewController.create()
        fishInfoVC.isInfinity = true
        
        var option = TabPageOption()
        option.currentColor = UIColor.fishMenuColor
        option.defaultColor = UIColor.fishMenuColor
        option.tabBackgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        option.pageBackgoundColor = UIColor.white
        option.fontSize = 30.0
        option.tabWidth = 340.0
        
        var viewControllersArray = [(UIViewController, String)]()
        
        let descriptionVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "fishDescriptionVC") as! FishDescriptionViewController
        descriptionVC.type = .description
        descriptionVC.fishIndex = self.selectedIndex
        viewControllersArray.append((descriptionVC, "Descripción"))
        
        let habitatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "fishHabitatVC") as! FishHabitatViewController
        habitatVC.fishIndex = self.selectedIndex
        viewControllersArray.append((habitatVC, "Hábitat"))
        
        let dietVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "fishDietVC") as! FishDietViewController
        dietVC.fishIndex = self.selectedIndex
        viewControllersArray.append((dietVC, "Alimentación"))
        
        if let _ = FishTank.sharedInstance.fishes[self.selectedIndex].extraData {
            let extraDataVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "fishDescriptionVC") as! FishDescriptionViewController
            extraDataVC.type = .extraData
            extraDataVC.fishIndex = self.selectedIndex
            viewControllersArray.append((extraDataVC, "Dato Curioso"))
            option.tabWidth = 290.0
        }
        
        fishInfoVC.tabItems = viewControllersArray
        fishInfoVC.option = option
        self.navigationController?.pushViewController(fishInfoVC, animated: true)
    }
    
    // MARK:- Collection View Delegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row - 1
        
        if self.selectedIndex >= 0 {
            self.createFishSlideShow()
        }
        else {
            let fishTankInfoVC = TabPageViewController.create()
            fishTankInfoVC.isInfinity = true
            
            var option = TabPageOption()
            option.currentColor = UIColor.fishMenuColor
            option.defaultColor = UIColor.fishMenuColor
            option.tabBackgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            option.pageBackgoundColor = UIColor.white
            option.fontSize = 30.0
            if FishTank.sharedInstance.type == .coralReef {
                option.tabWidth = 450.0
            } else {
                option.tabWidth = 340.0
            }
            
            var viewControllersArray = [(UIViewController, String)]()
            
            for (index, fishTankTitle) in FishTank.sharedInstance.fishTantkTitles.enumerated() {
                
                if index == (FishTank.sharedInstance.fishTantkTitles.count - 1) &&
                    FishTank.sharedInstance.fishTantkImageNames.count < FishTank.sharedInstance.fishTantkTitles.count {
                    let descriptionVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "fishTankDescNoImageVC") as! FishTankDescriptionViewController
                    descriptionVC.index = index
                    viewControllersArray.append((descriptionVC, fishTankTitle))
                } else {
                    let descriptionVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "fishTankDescriptionVC") as! FishTankDescriptionViewController
                    descriptionVC.index = index
                    viewControllersArray.append((descriptionVC, fishTankTitle))
                }
            }
            
            fishTankInfoVC.tabItems = viewControllersArray
            fishTankInfoVC.option = option
            self.navigationController?.pushViewController(fishTankInfoVC, animated: true)
        }
    }
    
    // MARK:- Collection view Data Source
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return FishTank.sharedInstance.fishes.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fishCell", for: indexPath) as! MenuCollectionViewCell
        if indexPath.row == 0 {
            cell.setCellForFishTank()
        }
        else {
            cell.setCell(for: FishTank.sharedInstance.fishes[indexPath.row - 1])
        }
        
        return cell
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}
