//
//  Planetario+UIViewController.swift
//  Planetario
//
//  Created by Alejandro de la Rosa on 4/5/19.
//  Copyright © 2019 Alex De la Rosa. All rights reserved.
//

import UIKit

extension UIViewController {
    var prefersStatusBarHidden: Bool {
        return true
    }
}
