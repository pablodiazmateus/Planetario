//
//  FishDietViewController.swift
//  Planetario
//
//  Created by Alex De la Rosa on 16/02/17.
//  Copyright © 2017 Alex De la Rosa. All rights reserved.
//

import UIKit

class FishDietViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var infoImage: UIImageView! {
        willSet {
            newValue.image = UIImage(named: FishTank.sharedInstance.fishes[self.fishIndex].imageName)
        }
    }
    
    @IBOutlet weak var infoText: UITextView! {
        willSet {
            newValue.textColor = UIColor.textColor
            newValue.backgroundColor = UIColor.textBackgroundColor
            newValue.text = "\(FishTank.sharedInstance.fishes[self.fishIndex].diet)"
        }
    }
    
    @IBOutlet weak var background: UIImageView! {
        willSet {
            newValue.image = UIImage(named: FishTank.sharedInstance.type.getBackgroundImageName())
        }
    }
    
    var fishIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.minimumZoomScale = 1.0;
        self.scrollView.maximumZoomScale = 6.0;
        self.scrollView.contentSize = self.infoImage.frame.size;
        self.scrollView.delegate = self;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.infoImage
    }
}
