//
//  FishTank.swift
//  Planetario
//
//  Created by Alex De la Rosa on 17/12/16.
//  Copyright © 2016 Alex De la Rosa. All rights reserved.
//

import UIKit

enum FishTankType: String {
    case amazon = "Amazon"
    case amazonPredators = "AmazonPredators"
    case coralReef = "CoralReef"
    case lakeVictoria = "LakeVictoria"
    case caribeanSea = "CaribeanSea"
    
    func getPropertyListName() -> String {
        switch self {
        case .amazon:
            return "AmazonFishTank"
        case .amazonPredators:
            return "AmazonPredatorsFishTank"
        case .coralReef:
            return "CoralReefFishTank"
        case .lakeVictoria:
            return "LakeVictoriaFishTank"
        case .caribeanSea:
            return "CaribeanSeaFishTank"
        }
    }
    
    func getBackgroundImageName() -> String {
        switch self {
        case .amazon, .amazonPredators:
            return FishTankType.amazon.rawValue
        default:
            return self.rawValue
        }
    }
}

class FishTank: NSObject {
    
    static let sharedInstance = FishTank()
    var fishTankName: String = ""
    var fishes: [Fish] = [Fish]()
    var type: FishTankType = .amazon
    var videoName: String = ""
    var fishTantkTitles: [String] = [String]()
    var fishTantkDescriptions: [String] = [String]()
    var fishTantkImageNames: [String] = [String]()
    
    func setFishTankData() {
        if let path = Bundle.main.path(forResource: self.type.getPropertyListName(), ofType: "plist") {
            if let fishTankData = NSDictionary(contentsOfFile: path) {
                if let fishTankName = fishTankData["fishTankName"] as? String {
                    self.fishTankName = fishTankName
                }
                if let fishesData = fishTankData["fishes"] as? [NSDictionary] {
                    var fishes: [Fish] = [Fish]()
                    for fishData in fishesData {
                        let fish = Fish(with: fishData)
                        fishes.append(fish)
                    }
                    self.fishes = fishes
                }
                if let video = fishTankData["fishTankVideo"] as? String {
                    self.videoName = video
                }
                if let fishTantkTitles = fishTankData["fishTankTitles"] as? [String] {
                    self.fishTantkTitles = fishTantkTitles
                }
                if let fishTantkDescriptions = fishTankData["fishTankDescriptions"] as? [String] {
                    self.fishTantkDescriptions = fishTantkDescriptions
                }
                if let fishTantkImageNames = fishTankData["fishTankImages"] as? [String] {
                    self.fishTantkImageNames = fishTantkImageNames
                }
            }
        }
    }
}
