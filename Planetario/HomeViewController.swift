//
//  HomeViewController.swift
//  Planetario
//
//  Created by Alex De la Rosa on 14/12/16.
//  Copyright © 2016 Alex De la Rosa. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class HomeViewController: UIViewController {
    
    weak private var player = AVPlayer()
    weak private var playerLayer = AVPlayerLayer()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.playVideo()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.player = nil
        self.playerLayer?.removeFromSuperlayer()
    }
    
    private func playVideo() {
        let videoURL: URL = Bundle.main.url(forResource: FishTank.sharedInstance.videoName, withExtension: "mp4")!
        
        let asset = AVAsset(url: videoURL)
        
        let assetKeys = [
            "playable"
        ]
        // Create a new AVPlayerItem with the asset and an
        // array of asset keys to be automatically loaded
        let playerItem = AVPlayerItem(asset: asset,
                                  automaticallyLoadedAssetKeys: assetKeys)
        
        // Associate the player item with the player
        self.player = AVPlayer(playerItem: playerItem)
        
        self.player?.actionAtItemEnd = .none
        self.player?.isMuted = true
        
        self.playerLayer = AVPlayerLayer(player: player)
        self.playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspect
        self.playerLayer?.zPosition = -1
        self.playerLayer?.frame = view.frame
        
        view.layer.addSublayer(self.playerLayer!)
        
        player?.play()
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: nil, using: { (_) in
            DispatchQueue.main.async {
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
                
            }
        })
    }
    
    @IBAction private func diPressToMenu() {
        self.performSegue(withIdentifier: "toMenu", sender: self)
    }
}
