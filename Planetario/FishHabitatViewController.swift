//
//  FishHabitatViewController.swift
//  Planetario
//
//  Created by Alex De la Rosa on 16/02/17.
//  Copyright © 2017 Alex De la Rosa. All rights reserved.
//

import UIKit

class FishHabitatViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var singleTap: UITapGestureRecognizer!
    
    @IBOutlet weak var infoImage: UIImageView! {
        willSet {
            newValue.image = UIImage(named: FishTank.sharedInstance.fishes[self.fishIndex].mapImage)
        }
    }
    
    @IBOutlet weak var infoText: UITextView! {
        willSet {
            newValue.textColor = UIColor.textColor
            newValue.backgroundColor = UIColor.textBackgroundColor
            newValue.text = "\(FishTank.sharedInstance.fishes[self.fishIndex].habitat)"
        }
    }
    
    @IBOutlet weak var background: UIImageView! {
        willSet {
            newValue.image = UIImage(named: FishTank.sharedInstance.type.getBackgroundImageName())
        }
    }
    
    var fishIndex = -1
    var isZoomIn = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if FishTank.sharedInstance.fishes[self.fishIndex].mapImage !=
            FishTank.sharedInstance.fishes[self.fishIndex].mapImageZoom {
            let doubleTap = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap(sender:)))
            doubleTap.numberOfTapsRequired = 2
            singleTap.require(toFail: doubleTap)
            self.scrollView.addGestureRecognizer(doubleTap)
            self.scrollView.maximumZoomScale = 1.0
        }
        else {
            self.scrollView.maximumZoomScale = 6.0
            self.infoImage.image = UIImage(named: FishTank.sharedInstance.fishes[self.fishIndex].mapImageZoom)
        }
        
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.contentSize = self.infoImage.frame.size
        self.scrollView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func handleDoubleTap(sender: UITapGestureRecognizer) {
        self.isZoomIn = !self.isZoomIn
        
        if(isZoomIn) {
            self.scrollView.zoomScale = 1.0
            self.scrollView.maximumZoomScale = 6.0
            self.infoImage.image = UIImage(named: FishTank.sharedInstance.fishes[self.fishIndex].mapImageZoom)
        }
        else {
            self.scrollView.zoomScale = 1.0
            self.scrollView.maximumZoomScale = 1.0
            self.infoImage.image = UIImage(named: FishTank.sharedInstance.fishes[self.fishIndex].mapImage)
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.infoImage
    }
}
