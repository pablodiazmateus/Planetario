//
//  Planetario + UIColor.swift
//  Planetario
//
//  Created by Alex De la Rosa on 03/01/17.
//  Copyright © 2017 Alex De la Rosa. All rights reserved.
//
import UIKit

extension UIColor {
    static var fishTankTitleColor: UIColor {
        switch FishTank.sharedInstance.type {
        case .amazon, .amazonPredators:
            return UIColor(hexString: "A1AA58")
        case .caribeanSea:
            return UIColor(hexString: "3E8D9C")
        case .coralReef:
            return UIColor(hexString: "3DD6EA")
        case .lakeVictoria:
            return UIColor(hexString: "125690")
        }
    }
    
    static var fishMenuColor: UIColor {
        switch FishTank.sharedInstance.type {
        case .amazon, .amazonPredators:
            return UIColor(hexString: "3776B7")
        case .caribeanSea:
            return UIColor(hexString: "A91E5A")
        case .coralReef:
            return UIColor(hexString: "ED0A7F")
        case .lakeVictoria:
            return UIColor(hexString: "9D3A36")
        }
    }
    
    static var textBackgroundColor: UIColor {
        switch FishTank.sharedInstance.type {
        case .amazon, .amazonPredators:
            return UIColor(hexString: "A1AA58")
        case .caribeanSea:
            return UIColor(hexString: "3E8D9C")
        case .coralReef:
            return UIColor(hexString: "3ACBDE")
        case .lakeVictoria:
            return UIColor(hexString: "125690")
        }
    }
    
    static var textColor: UIColor {
        switch FishTank.sharedInstance.type {
        case .amazon, .amazonPredators:
            return UIColor(hexString: "773632")
        case .caribeanSea:
            return UIColor(hexString: "EDEDD7")
        case .coralReef:
            return UIColor(hexString: "820263")
        case .lakeVictoria:
            return UIColor(hexString: "FFF172")
        }
    }
    
    static var backTextColor: UIColor {
        switch FishTank.sharedInstance.type {
        case .amazon, .amazonPredators:
            return UIColor(hexString: "F61101")
        case .caribeanSea:
            return UIColor(hexString: "F61101")
        case .coralReef:
            return UIColor(hexString: "40C425")
        case .lakeVictoria:
            return UIColor(hexString: "40C425")
        }
    }
    
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
