//
//  Fish.swift
//  Planetario
//
//  Created by Alex De la Rosa on 17/12/16.
//  Copyright © 2016 Alex De la Rosa. All rights reserved.
//

import UIKit

class Fish: NSObject {
    
    var name: String
    var scientificName: String
    var fishDescription: String
    var diet: String
    var habitat: String
    var mapImage: String
    var mapImageZoom: String
    var photoName: String
    var imageName: String
    var extraData: String?
    var extraDataImage: String?
    
    init(with fishData: NSDictionary) {
        if let name = fishData["name"] as? String {
            self.name = name
        } else {
            self.name = ""
        }
        if let scientificName = fishData["scientificName"] as? String {
            self.scientificName = scientificName
        } else {
            self.scientificName = ""
        }
        if let fishDescription = fishData["fishDescription"] as? String {
            self.fishDescription = fishDescription
        } else {
            self.fishDescription = ""
        }
        if let diet = fishData["diet"] as? String {
            self.diet = diet
        } else {
            self.diet = ""
        }
        if let habitat = fishData["habitat"] as? String {
            self.habitat = habitat
        } else {
            self.habitat = ""
        }
        if let mapImage = fishData["mapImage"] as? String {
            self.mapImage = mapImage
        } else {
            self.mapImage = ""
        }
        if let mapImageZoom = fishData["mapImageZ"] as? String {
            self.mapImageZoom = mapImageZoom
        } else {
            self.mapImageZoom = ""
        }
        if let photoName = fishData["photoName"] as? String {
            self.photoName = photoName
        } else {
            self.photoName = ""
        }
        if let imageName = fishData["imageName"] as? String {
            self.imageName = imageName
        } else {
            self.imageName = ""
        }
        if let extraData = fishData["extraData"] as? String {
            self.extraData = extraData
        } else {
            self.extraData = nil
        }
        if let extraDataImage = fishData["extraDataImage"] as? String {
            self.extraDataImage = extraDataImage
        } else {
            self.extraDataImage = nil
        }
        
    }
    
    override init() {
        self.name = ""
        self.scientificName = ""
        self.fishDescription = ""
        self.diet = ""
        self.habitat = ""
        self.mapImage = ""
        self.mapImageZoom = ""
        self.photoName = ""
        self.imageName = ""
        self.extraData = nil
        self.extraDataImage = nil
        super.init()
    }
}
