//
//  PlanetarioApplication.swift
//  Planetario
//
//  Created by Alex De la Rosa on 27/02/17.
//  Copyright © 2017 Alex De la Rosa. All rights reserved.
//

import Foundation
import UIKit

class PlanetarioApplication: UIApplication {
    
    static let ApplicationDidTimeoutNotification = "AppTimeout"
    
    // The timeout in seconds for when to fire the idle timer.
    private let timeoutInSeconds: TimeInterval = 45
    private var idleTimer: Timer?
    
    // Listen for any touch. If the screen receives a touch, the timer is reset.
    override func sendEvent(_ event: UIEvent) {
        super.sendEvent(event)
        
        if idleTimer != nil {
            self.resetIdleTimer()
        }
        
        if let touches = event.allTouches {
            for touch in touches {
                if touch.phase == UITouch.Phase.began {
                    self.resetIdleTimer()
                }
            }
        }
    }
    
    // Resent the timer because there was user interaction.
    private func resetIdleTimer() {
        if let idleTimer = idleTimer {
            idleTimer.invalidate()
        }
        
        self.idleTimer = Timer.scheduledTimer(timeInterval: timeoutInSeconds, target: self, selector: #selector(PlanetarioApplication.idleTimerExceeded), userInfo: nil, repeats: false)
    }
    
    // If the timer reaches the limit as defined in timeoutInSeconds, post this notification.
    @objc private func idleTimerExceeded() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: PlanetarioApplication.ApplicationDidTimeoutNotification), object: nil)
    }
}
