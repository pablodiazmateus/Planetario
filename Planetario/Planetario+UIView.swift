//
//  Planetario+UIView.swift
//  Planetario
//
//  Created by Alejandro de la Rosa on 4/5/19.
//  Copyright © 2019 Alex De la Rosa. All rights reserved.
//

import UIKit

extension UIView {
    func roundAllCorners(with radius: CGFloat = 6.0) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}
