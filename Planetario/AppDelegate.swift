//
//  AppDelegate.swift
//  Planetario
//
//  Created by Alex De la Rosa on 25/11/16.
//  Copyright © 2016 Alex De la Rosa. All rights reserved.
//

import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.applicationDidTimeout(notification:)), name: NSNotification.Name(rawValue: PlanetarioApplication.ApplicationDidTimeoutNotification), object: nil)
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if let fishTankType = UserDefaults.standard.value(forKey: "fishTankPreference") as? String, let type = FishTankType(rawValue: fishTankType) {
            FishTank.sharedInstance.type = type
        }
        
        FishTank.sharedInstance.setFishTankData()
        
        self.moveToVideo()
    }
    
    /**
     This method move the rootViewController to a new instance of `HomeViewController` in order to reload the Fish Tank Video.
     */
    private func moveToVideo() {
        let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
        self.window?.rootViewController = homeVC
    }
    
    /**
     This method is executed each time the App Delegate receives an `ApplicationDidTimeOutNotification`.
     Calls the `moveToVideo` method that reloads the Fish Tank video.
     - Parameter notification: The notification that is received.
     */
    @objc private func applicationDidTimeout(notification: NSNotification) {
        self.moveToVideo()
    }
}



