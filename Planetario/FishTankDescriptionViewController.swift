//
//  FishTankDescriptionViewController.swift
//  Planetario
//
//  Created by Alex De la Rosa on 22/02/17.
//  Copyright © 2017 Alex De la Rosa. All rights reserved.
//

import UIKit

class FishTankDescriptionViewController: UIViewController {
    
    @IBOutlet weak var infoImage: UIImageView! {
        willSet {
            if self.index < FishTank.sharedInstance.fishTantkImageNames.count {
                newValue.image = UIImage(named: FishTank.sharedInstance.fishTantkImageNames[self.index])
            }
        }
    }
    
    @IBOutlet weak var infoText: UITextView! {
        willSet {
            newValue.textColor = UIColor.textColor
            newValue.backgroundColor = UIColor.textBackgroundColor
            newValue.text = FishTank.sharedInstance.fishTantkDescriptions[self.index]
        }
    }
    
    @IBOutlet weak var background: UIImageView! {
        willSet {
            newValue.image = UIImage(named: FishTank.sharedInstance.type.getBackgroundImageName())
        }
    }
    
    var index = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func handlePinchGesture(sender: UIPinchGestureRecognizer) {
        let factor: CGFloat = sender.scale
        sender.view?.transform = CGAffineTransform(scaleX: factor, y: factor)
    }
}
