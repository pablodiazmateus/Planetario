//
//  FishDescriptionViewController.swift
//  Planetario
//
//  Created by Alex De la Rosa on 16/02/17.
//  Copyright © 2017 Alex De la Rosa. All rights reserved.
//
extension String {
    func capitalizeFirst() -> String {
        let firstIndex = self.index(startIndex, offsetBy: 1)
        return self[..<firstIndex].capitalized + self[firstIndex...].lowercased()
    }
}

import UIKit

enum DescriptionType {
    case description
    case extraData
}

class FishDescriptionViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var infoImage: UIImageView! {
        willSet {
            if self.type == .description {
                newValue.image = UIImage(named: FishTank.sharedInstance.fishes[self.fishIndex].imageName)
            }
            else if self.type == .extraData {
                newValue.image = UIImage(named: FishTank.sharedInstance.fishes[self.fishIndex].extraDataImage!)
            }
        }
    }
    
    @IBOutlet weak var infoText: UITextView! {
        willSet {
            newValue.textColor = UIColor.textColor
            newValue.backgroundColor = UIColor.textBackgroundColor
            if self.type == .description {
                let fishName = "\(FishTank.sharedInstance.fishes[self.fishIndex].name)\n"
                let boldText = [NSAttributedString.Key.font: UIFont(name: "Raleway-Bold", size: 28.0)!,
                                NSAttributedString.Key.foregroundColor: UIColor.textColor]
                let fishDescription = NSMutableAttributedString(string: fishName, attributes: boldText)
                
                let scientificName = "\(FishTank.sharedInstance.fishes[self.fishIndex].scientificName)\n".capitalizeFirst()
                let italicText = [NSAttributedString.Key.font: UIFont(name: "Raleway-Italic", size: 28.0)!,
                                  NSAttributedString.Key.foregroundColor: UIColor.textColor]
                let fishScientificName = NSMutableAttributedString(string: scientificName, attributes: italicText)
                fishDescription.append(fishScientificName)
                
                let description = "\(FishTank.sharedInstance.fishes[self.fishIndex].fishDescription)"
                let normalText = [NSAttributedString.Key.font: UIFont(name: "Raleway", size: 28.0)!,
                                  NSAttributedString.Key.foregroundColor: UIColor.textColor]
                let descriptionText = NSMutableAttributedString(string: description, attributes: normalText)
                fishDescription.append(descriptionText)
                newValue.attributedText = fishDescription
            }
            else if self.type == .extraData {
                newValue.text = "\(FishTank.sharedInstance.fishes[self.fishIndex].extraData!)"
            }
        }
    }
    
    @IBOutlet weak var background: UIImageView! {
        willSet {
            newValue.image = UIImage(named: FishTank.sharedInstance.type.getBackgroundImageName())
        }
    }
    
    var fishIndex = -1
    var type: DescriptionType = .description

    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.minimumZoomScale = 1.0;
        self.scrollView.maximumZoomScale = 6.0;
        self.scrollView.contentSize = self.infoImage.frame.size;
        self.scrollView.delegate = self;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.infoImage
    }
}
