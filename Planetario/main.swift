//
//  main.swift
//  Planetario
//
//  Created by Alex De la Rosa on 27/02/17.
//  Copyright © 2017 Alex De la Rosa. All rights reserved.
//

import UIKit

let _ = UIApplicationMain(CommandLine.argc, CommandLine.unsafeArgv, NSStringFromClass(PlanetarioApplication.self), NSStringFromClass(AppDelegate.self))
